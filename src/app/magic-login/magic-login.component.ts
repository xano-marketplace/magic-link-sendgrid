import { Component, OnInit } from '@angular/core'
import { ActivatedRoute, Router } from '@angular/router'
import { ConfigService } from 'src/app/_demo-core/config.service'
import { mergeMap } from 'rxjs/operators'
import { EMPTY } from 'rxjs'

@Component({
  selector: 'app-magic-login',
  templateUrl: './magic-login.component.html'
})
export class MagicLoginComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private configService: ConfigService
  ) {
  }

  ngOnInit(): void {
    this.route.queryParams.pipe(mergeMap((params)=> {
      if(params.token) {
        return this.configService.xanoAPI('/auth/magic-login', 'post', {
          magic_token: params.token
        })
      } else {
        return EMPTY
      }
    })).subscribe(token => {
      if(token) {
        this.configService.authToken.next(token);
        this.router.navigate(['guarded-route'])
      }
    }, error => {
      this.configService.showErrorSnack(error);
      this.router.navigate(['home'])
    });
  }

}
