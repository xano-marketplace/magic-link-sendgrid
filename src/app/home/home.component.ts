import {Component, OnInit} from '@angular/core';
import {ConfigService} from "../_demo-core/config.service";
import { DemoConfig } from 'src/app/_demo-core/_demo-core'
import { ControlSchema } from 'src/app/_demo-core/form-generator/form-generator.component'
import { FormGroup } from '@angular/forms'
import { Router } from '@angular/router'
import { MatSnackBar } from '@angular/material/snack-bar'

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
	constructor(
		private configService: ConfigService,
		private router: Router,
		private snackBar: MatSnackBar
	) {
	}

	public config: DemoConfig;
	public configured: boolean = false;

	public form: FormGroup;
	public signupForm: FormGroup;
	public magicLinkSent: boolean = false;
	public showSignup: boolean = false;
	public formSchema: ControlSchema = {
		name: 'email',
		type: 'email',
		label: 'Email',
		validators: ['email'],
		icon: 'mail',
	};

	public signupFormSchema: ControlSchema[] = [
		{name: 'name', type: 'text', label: 'Name', validators: ['required'], icon: 'person'},
		{name: 'email', type: 'email', label: 'Email', validators: ['email', 'required'], icon: 'mail'},
		{name: 'password', type: 'password', label: 'Password', icon: 'lock', validators: ['required']},
	];



	ngOnInit(): void {
		if (this.configService.authToken.value) {
			this.router.navigate(['guarded-route'])
		}
		this.config = this.configService.config;
		this.configService.xanoApiUrl.subscribe(apiUrl => this.configured = !!apiUrl)
	}

	public requestMagicLink() {
		this.form.markAllAsTouched();
		if(this.form.valid) {
			this.configService.xanoAPI(
				'/auth/magic-link',
				'get',
				null,
				{params: {...this.form.getRawValue()}}
			).subscribe(
				res => this.magicLinkSent = true,
				error => this.configService.showErrorSnack(error));
		}
	}

	public signup() {
		this.signupForm.markAllAsTouched();
		if(this.signupForm.valid) {
			this.configService.xanoAPI('/auth/signup', 'post', this.signupForm.getRawValue())
				.subscribe(token => {
					if(token) {
						this.snackBar.open("You're signed up! You can now request a magic link!", 'Success');
						this.showSignup = false;
					}
				},
				error => this.configService.showErrorSnack(error));
		}
	}

}
